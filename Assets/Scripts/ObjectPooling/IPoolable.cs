﻿namespace ObjectPooling
{
    public interface IPoolable
    {
        void OnReleasedFromPool();

        void OnTakenByPool();
    }
}