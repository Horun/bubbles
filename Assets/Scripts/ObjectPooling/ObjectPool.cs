﻿using System.Collections.Generic;
using UnityEngine;

namespace ObjectPooling
{
    public abstract class ObjectPool<T>: MonoBehaviour  where T : Component
    {
        public int StartWidth = 5;
        protected abstract T GetPrefab { get; }

        private List<T> _pool = new List<T>();

        void Start()
        {
            Expand(StartWidth);
        }

        private void Expand(int count)
        {
            for (int i = 0; i < count; i++)
            {
                var newObj = Instantiate(GetPrefab);
                newObj.gameObject.SetActive(false);
                newObj.transform.SetParent(transform);
                _pool.Add(newObj);
                OnObjectMade(newObj);
            }
        }

        protected abstract void OnObjectMade(T obj);

        public T GetObject()
        {
            if(_pool.Count == 0)
                Expand(1);

            var gettingObj = _pool[_pool.Count - 1];
            _pool.RemoveAt(_pool.Count-1);
    
            gettingObj.gameObject.SetActive(true);

            OnObjectReleased(gettingObj);

            return gettingObj;
        }

        protected virtual void OnObjectReleased(T obj)
        {
            var iPoolable = obj as IPoolable;
            if (iPoolable != null) iPoolable.OnReleasedFromPool();
        }

        public void TakeObject(T obj)
        {
#if UNITY_EDITOR
            if (_pool.Contains(obj))
            {
                Debug.LogError("Try put object in pool twice");
                return;
            }
#endif

            _pool.Add(obj); 

            OnObjectTaken(obj);

            obj.gameObject.SetActive(false);
        }

        protected virtual void OnObjectTaken(T obj)
        {
            var iPoolable = obj as IPoolable;
            if (iPoolable != null) iPoolable.OnTakenByPool();
        }
    }
}
