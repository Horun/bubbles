﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace GameManaging
{
    public class ScoreDisplay : MonoBehaviour
    {
        public Text Display;
        public ScoreCenter ScoreCenter;

        void Start()
        {
            Refresh(ScoreCenter.CurrentScore);
            ScoreCenter.ScoreChanged += Refresh;
        }

        private void Refresh(float score)
        {
            Display.text = Math.Round(score,2).ToString();
        }
    }
}
