﻿using BubbleManaging;
using UnityEngine;

namespace GameManaging
{
    public class PlayerControl : MonoBehaviour
    {
        void Update()
        {
            if(!Input.GetMouseButtonDown(0)) return;

            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            var hit = Physics2D.Raycast(ray.origin, ray.direction, 100);
            if (hit.collider != null)
            {
                CheckHitObject(hit.collider);
            }
        }

        private void CheckHitObject(Collider2D coll)
        {
            var bubble = coll.GetComponent<Bubble>();
            if (bubble != null)
            {
                bubble.Die();
            }
        }
    }
}