﻿using System;
using BubbleManaging;
using UnityEngine;

namespace GameManaging
{
    public class ScoreCenter : MonoBehaviour
    {
        public float CurrentScore
        {
            get { return _currentScore; }
            set
            {
                if(_currentScore == value) return;

                _currentScore = value;
                if(ScoreChanged!=null) ScoreChanged.Invoke(_currentScore);
            }
        }

        private float _currentScore;

        public Action<float> ScoreChanged;

        public BubbleGenerator BubbleGenerator;

        void Start()
        {
            BubbleGenerator.BubbleReleased += OnBubbleReleased;
        }

        private void OnBubbleReleased(Bubble bubble)
        {
            bubble.Dead +=(playerKill)=> CalculateScore(bubble, playerKill);
        }

        private void CalculateScore(Bubble bubble, bool playerKill)
        {
            if (playerKill)
                CurrentScore += bubble.ScorePoints;
            else
                CurrentScore -= bubble.ScorePoints;

        }
    }
}
