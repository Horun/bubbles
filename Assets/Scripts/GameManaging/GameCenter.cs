﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameManaging
{
    public class GameCenter : MonoBehaviour
    {
        public void StartGame()
        {

        }

        public void RestartGame()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
