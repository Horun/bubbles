﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace BubbleManaging
{
    public class BubbleGenerator : MonoBehaviour
    {
        public float MinBubbleSize = 0.3f;
        public float MaxBubbleSize = 2f;

        public float MinTimeInterval = 0.1f;
        public float MaxTimeInterval = 0.7f;

        public float GenerationAreaWidth = 4f;

        public BubblesPool BubblesPool;
        public Action<Bubble> BubbleReleased;

        private float GenerTimeInterval
        {
            get { return Random.Range(MinTimeInterval, MaxTimeInterval); }
        }

        private IEnumerator Start()
        {
            while (true)
            {
                yield return new WaitForSecondsRealtime(GenerTimeInterval);
                GenerateBubble();
            }
        }

        private void GenerateBubble()
        {
            var bubble = BubblesPool.GetObject();
            bubble.transform.position = transform.position + new Vector3(Random.Range(-GenerationAreaWidth/2f, GenerationAreaWidth/2f),0,0);
            bubble.Size = Random.Range(MinBubbleSize, MaxBubbleSize);
            BubbleReleased.Invoke(bubble);
        }

        void OnValidate()
        {
            if (MaxBubbleSize < MinBubbleSize)
            {
                MaxBubbleSize = MinBubbleSize+ 0.1f;
            }

            if (MinTimeInterval > MaxTimeInterval)
            {
                MaxTimeInterval = MinTimeInterval + 0.1f;
            }
        }
    }
}
