﻿using UnityEngine;

namespace BubbleManaging
{
    public class DeathEffect : MonoBehaviour
    {
        public GameObject DeathParticle;

        void Start()
        {
            GetComponent<Bubble>().Dead += (x) =>
            {
                var effect = Instantiate(DeathParticle, transform.position, Quaternion.identity);
                Destroy(effect, 5f);
            };
        }

    }
}
