﻿using System;
using UnityEngine;

namespace BubbleManaging
{
    public class Bubble : MonoBehaviour
    {
        public float ScorePoints { get{return (float)Math.Round(Size, 2); } }

        public float Size
        {
            get { return _currentSize;}
            set
            {
                _currentSize = value;
                transform.localScale = _defaultScale * value;
            } }
        
        public Action<bool> Dead;

        private Vector3 _defaultScale;
        private float _currentSize;

        public void Init()
        {
            _defaultScale = transform.localScale;
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            OnDead(false);
        }

        public void Die()
        {
            OnDead(true);
        }

        private void OnDead(bool isKilledByUser)
        {
            gameObject.SetActive(false);
            if (Dead != null) Dead.Invoke(isKilledByUser);
        }
    }
}
