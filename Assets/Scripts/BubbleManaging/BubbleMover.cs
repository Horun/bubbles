﻿using UnityEngine;

namespace BubbleManaging
{
    [RequireComponent(typeof(Bubble))]
    public class BubbleMover: MonoBehaviour
    {
        public float SpeedSizeRatio = 1;

        private Bubble _bubble;

        void Start()
        {
            _bubble = GetComponent<Bubble>();
        }

        void Update()
        {
            transform.position += Vector3.up * SpeedSizeRatio * _bubble.Size;
        }
    }
}