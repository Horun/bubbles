﻿using ObjectPooling;
using UnityEngine;

namespace BubbleManaging
{
    public class BubblesPool : ObjectPool<Bubble>
    {
        [SerializeField]
        private Bubble _prefab;

        protected override Bubble GetPrefab
        {
            get { return _prefab; }
        }

        protected override void OnObjectMade(Bubble obj)
        {
            obj.Init();
            obj.Dead += x => TakeObject(obj);
        }
    }
}
